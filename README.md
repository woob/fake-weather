# fake-weather site scraping demo

This is a fake weather site, that can be used for practising basic scraping.

On this site, cities can be searched.
for each city the current weather, temperature and wind speed can be fetched, along with forecast.
