#!/usr/bin/env python3
# SPDX-License-Identifier: MIT

from dataclasses import dataclass
import datetime
import math
from pathlib import Path

from bottle import get, run, request, redirect, template, static_file


EPOCH = datetime.date(1970, 1, 1)


def fakerandom(date, modulo):
    seed = (date - EPOCH).days
    return int(math.cos(seed) * 10000) % modulo


@dataclass
class City:
    id: int
    name: str
    base_temperature: int
    weather: list[str]
    max_wind: int


STATIC_DIR = Path(__file__).with_name("static")


def mytemplate(name, **kwargs):
    template_text = STATIC_DIR.joinpath(name).read_text()
    return template(template_text, **kwargs)


@get("/")
def _():
    redirect("/index")


@get("/index")
def _():
    return static_file("index.html", STATIC_DIR)


def build_day_temperature(date, city):
    low_temperature = city.base_temperature - 10
    high_temperature = city.base_temperature + 10


    date = date - datetime.timedelta(days=7 * 6)
    base_date = datetime.date(date.year, 1, 1)
    dayindex = (date - base_date).days
    dayindex /= (365/2)
    if dayindex <= 1:
        temp = low_temperature + (high_temperature - low_temperature) * dayindex
    else:
        temp = low_temperature + (high_temperature - low_temperature) * (2 - dayindex)

    temp = round(temp * 2, 0) / 2
    temp += ((date.day % 3) - 1) / 2
    return temp


CITIES_LIST = [
    City(
        id=1, name="Paris", base_temperature=20, weather=["sun-light", "rain"],
        max_wind=2,
    ),
    City(
        id=2, name="London", base_temperature=10, weather=["rain"],
        max_wind=3,
    ),
    City(
        id=3, name="Barcelona", base_temperature=30, weather=["sun-light"],
        max_wind=2,
    ),
]

CITIES = {
    city.id: city for city in CITIES_LIST
}


@get("/cities")
def _():
    return mytemplate("search.html.tmpl", results=CITIES_LIST)


@get("/search")
def _():
    try:
        pattern = request.params["name"].lower()
    except KeyError:
        return mytemplate("error.html.tmpl", message="Missing 'name' parameter")

    matches = [
        city
        for city in CITIES_LIST
        if pattern in city.name.lower()
    ]
    return mytemplate("search.html.tmpl", results=matches)


def build_weather(date, city):
    temp = build_day_temperature(date, city)
    return {
        "weather": city.weather[fakerandom(date, len(city.weather))],
        "wind": fakerandom(date, city.max_wind + 1) * 10,
        "temperature": temp,
        "min": temp - 2,
        "max": temp + 2,
    }


@get("/current")
def _():
    try:
        city_id = int(request.params["city_id"])
        city = CITIES[city_id]
    except (ValueError, KeyError):
        return mytemplate("error.html.tmpl", message="Bad 'city_id' parameter")

    today = datetime.date.today()
    return mytemplate("current-temperature.html.tmpl", city=city, **build_weather(today, city))


@get("/forecast")
def _():
    try:
        city_id = int(request.params["city_id"])
        city = CITIES[city_id]
    except (ValueError, KeyError):
        return mytemplate("error.html.tmpl", message="Bad 'city_id' parameter")

    today = datetime.date.today()
    forecast = [
        {
            "date": today + datetime.timedelta(days=days),
            **build_weather(today + datetime.timedelta(days=days), city)
        }
        for days in range(1, 7)
    ]

    return mytemplate("forecast.html.tmpl", city=city, forecast=forecast)


@get("/<name>.svg")
def _(name):
    return static_file(f"{name}.svg", STATIC_DIR)


@get("/<name>.css")
def _(name):
    return static_file(f"{name}.css", STATIC_DIR)


if __name__ == "__main__":
    run(port=8000, debug=True, reloader=True)
